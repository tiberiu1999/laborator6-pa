package com.company;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import static com.company.DrawingPanel.H;
import static com.company.DrawingPanel.W;

public class ControlPanel extends JPanel {
    final MainFrame frame;
    JButton saveButton = new JButton("save");
    JButton loadButton = new JButton("load");
    JButton resetButton = new JButton("reset");
    JButton exitButton = new JButton("exit");

    public ControlPanel(MainFrame frame) {
        this.frame = frame;
        init();
    }

    private void init() {
        setLayout(new GridLayout(1, 4));
        //add all buttons
        add(saveButton);
        add(loadButton);
        add(resetButton);
        add(exitButton);

        //configure listeners for all buttons

        saveButton.addActionListener(this::save);
        loadButton.addActionListener(this::load);
        resetButton.addActionListener(this::reset);
        exitButton.addActionListener(this::exit);
    }

    private void save(ActionEvent e) {
        try {
            ImageIO.write(frame.canvas.image, "PNG", new File("test.png"));
        } catch (
                IOException ex) {
            System.err.println(ex);
        }
    }
    private void load(ActionEvent e) {
        try {
            File loadedImage = new File("test.png");
            frame.canvas.image = ImageIO.read(loadedImage);
            frame.canvas.graphics = frame.canvas.image.createGraphics();
        } catch (IOException ex) {
            System.err.println(ex);
        }
        repaint();
    }

    private void reset(ActionEvent e){
        frame.canvas.image = new BufferedImage(W, H, BufferedImage.TYPE_INT_ARGB);
        frame.canvas.graphics = frame.canvas.image.createGraphics();
        frame.canvas.graphics.setColor(Color.WHITE);
        frame.canvas.graphics.fillRect(0,0,W,H);
    }

    private void exit(ActionEvent e){
        System.exit(3);
    }
}