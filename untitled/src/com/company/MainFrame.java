package com.company;

import javax.swing.*;

import java.awt.*;

import static javax.swing.SwingConstants.*;

public class MainFrame extends JFrame {
    ConfigPanel configPanel;
    com.company.ControlPanel controlPanel;
    DrawingPanel canvas;

    public MainFrame(){
        super("My drawing app");
        init();
    }

    private void init() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //create the components
        canvas = new DrawingPanel(this);
        configPanel = new ConfigPanel(this);
        controlPanel = new com.company.ControlPanel(this);

        //arrange the components in the container(frame)
        //JFrame uses a BorderLayout by default
        add(canvas, CENTER);
        add(configPanel, BorderLayout.NORTH);
        add(controlPanel,BorderLayout.SOUTH);

        //invoke the layout manageer
        pack();
    }
}